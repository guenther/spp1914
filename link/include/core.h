#ifndef _CORE_H_
#define _CORE_H_

#include <arpa/inet.h>
#include <sys/types.h>

#define MSG_FLAG_ERASURE (1 << 0)
#define MSG_MAX_LEN 65536


/*
 * Messages consist of a 9B header (see below) followed by simulation-specific
 * body the network is unaware of. The message header looks as follows:
 *
         message header         message body
  |---------------------------|----- .. -----|
 * version   flags   timestamp
 *          76543210    63..0
 *          |||||||+ indicates that the corresponding message should considered be lost
 *          ||||||+- future use
 *          |||||+-- future use
 *          ||||+--- future use
 *          |||+---- future use
 *          ||+----- future use
 *          |+------ future use
 *          +------- future use
 *
 * Version is set to 0 for now and may be used in future to indicate updates of
 * the common message format.
 */

struct msg_header {
	uint8_t  version;
	uint8_t  flags;
	uint32_t timestamp;
	uint8_t  body[0];
} __attribute__((packed));

#endif
