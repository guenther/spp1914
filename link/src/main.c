/*
 * Copyright 2018		Stephan M. Guenther <guenther@tum.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * See COPYING for more details.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <argp.h>
#include <endian.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <arpa/inet.h>

#include <moepcommon/util.h>
#include <core.h>

#define MSG_MAX_LEN 65536


static char doc[] =
"link - a link simulator\n\n"
"  FROM_ADDR                  IP address used to receive UDP datagrams, e.g.\n"
"                             0.0.0.0 for any local address\n"
"  FROM_PORT                  UDP port used to receive UDP datagrams\n"
"  TO_ADDR                    IP address to which datagrams should be\n"
"                             forwarded, e.g. 127.0.0.1 for localhost\n"
"                             0.0.0.0 for any local address\n"
"  TO_PORT                    UDO port to which datagrams should be forwarded\n";

static char args_doc[] = "FROM_ADDR FROM_PORT TO_ADDR TO_PORT";

enum fix_args {
	FIX_ARG_FROM_ADDR = 0,
	FIX_ARG_FROM_PORT = 1,
	FIX_ARG_TO_ADDR = 2,
	FIX_ARG_TO_PORT = 3,
	FIX_ARG_CNT
};

static struct argp_option options[] = {
	{"erasure", 'e', "LOSS", 0, "Packet erasure probability between 0.0 and 1.0"},
	{"delay", 'd', "DELAY", 0, "Fixed delay in simulation ticks"},
	{"seed", 's', "SEED", 0, "Seed the PRNG for message erasures"},
	{}
};

static error_t parse_opt(int key, char *arg, struct argp_state *state);

static struct argp argp = {
	options,
	parse_opt,
	args_doc,
	doc
};

struct arguments {
	struct in_addr from_addr;
	unsigned short from_port;
	struct in_addr to_addr;
	unsigned short to_port;
	double erasure;
	uint32_t delay;
} args;

struct dgram {
	struct sockaddr_in sa;
	unsigned char payload[MSG_MAX_LEN];
	size_t len;
};

static sig_atomic_t _run = 1;


static void signal_handler(int signo)
{
	switch (signo)
	{
	case SIGINT:
	case SIGTERM:
		fprintf(stderr, "Received signal %d, shutting down ...\n",
									signo);
		_run = 0;
		break;
	default:
		fprintf(stderr, "Received signal %d, ignoring ...\n", signo);
	}
}


static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct arguments *args = state->input;
	int port, delay;

	switch (key) {
	case 'e':
		args->erasure = strtod(arg, NULL);
		if (args->erasure > 1.0 || args->erasure < 0.0)
			argp_failure(state, 1, errno, "Invalid erasure probability");
		break;
	case 'd':
		delay = atoi(arg);
		if (delay < 0)
			argp_failure(state, 1, errno, "Invalid delay");
		args->delay = (uint32_t)delay;
		break;
	case 's':
		srand(atoi(arg));
		break;
	case ARGP_KEY_ARG:
		switch (state->arg_num) {
		case FIX_ARG_FROM_ADDR:
			if (!inet_aton(arg, &args->from_addr))
				argp_failure(state, 1, errno, "Invalid from address");
			break;
		case FIX_ARG_FROM_PORT:
			port = atoi(arg);
			if (port < 1024 || port > 65535)
				argp_failure(state, 1, errno, "Invalid from port");
			args->from_port = (unsigned short)port;
			break;
		case FIX_ARG_TO_ADDR:
			if (!inet_aton(arg, &args->to_addr))
				argp_failure(state, 1, errno, "Invalid to address");
			break;
		case FIX_ARG_TO_PORT:
			port = atoi(arg);
			if (port < 1024 || port > 65535)
				argp_failure(state, 1, errno, "Invalid to port");
			args->to_port = (unsigned short)port;
			break;
		default:
			argp_usage(state);
		}
		break;
	case ARGP_KEY_END:
		if (state->arg_num < FIX_ARG_CNT)
			argp_usage(state);
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}


static int process_message(uint8_t *buffer, ssize_t len)
{
		struct msg_header *hdr = (struct msg_header *)buffer;
		uint32_t ts;
		int delay = -1;

		if (len < sizeof(*hdr)) {
				errno = ENOMSG;
				return -1;
		}
		if (hdr->version != 1) {
				errno = EBADMSG;
				return -1;
		}

		ts = hdr->timestamp;

		if (rand() < args.erasure) {
				hdr->flags |= MSG_FLAG_ERASURE;
				goto out;
		}

		delay = args.delay;		// constant for now
		hdr->timestamp = ts + delay;

		out:
		LOG(LOG_INFO, "%010u  %d  %d", ts, delay,
				hdr->flags & MSG_FLAG_ERASURE);

		return 0;
}


static void run()
{
	struct sockaddr_in safrom, sato;
	int sdfrom, sdto, maxfd, ret;
	uint8_t buffer[MSG_MAX_LEN];
	ssize_t len;
	socklen_t slen;
	fd_set rfd, rfds;

	signal(SIGPIPE, SIG_IGN);
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);

	slen = sizeof(safrom);
	memset(&sato, 0, sizeof(sato));
	memset(&safrom, 0, sizeof(safrom));

	safrom.sin_addr.s_addr = args.from_addr.s_addr;
	safrom.sin_port = htons(args.from_port);
	safrom.sin_family = AF_INET;
	sato.sin_addr.s_addr = args.to_addr.s_addr;
	sato.sin_port = htons(args.to_port);
	sato.sin_family = AF_INET;

	if (0 > (sdfrom = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))) {
		perror("socket() failed");
		exit(1);
	}
	if (0 > (sdto = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))) {
		perror("socket() failed");
		exit(1);
	}

	if (0 > bind(sdfrom, (struct sockaddr *)&safrom, sizeof(safrom))) {
		perror("bind() failed");
		exit(1);
	}

	FD_ZERO(&rfds);
	FD_SET(sdfrom, &rfds);
	maxfd = sdfrom;

	while (_run) {
		memset(buffer, 0, sizeof(buffer));
		rfd = rfds;

		ret = select(maxfd+1, &rfd, NULL, NULL, NULL);

		if (0 > ret) {
			if (errno == EINTR)
				continue;
			perror("select() failed");
			exit(1);
		}
		if (0 == ret) {
			continue;
		}

		if (!FD_ISSET(sdfrom, &rfd))
			continue;

		len = recvfrom(sdfrom, buffer, sizeof(buffer), MSG_DONTWAIT,
				(struct sockaddr *)&safrom, &slen);

		if (0 > len) {
			if (errno == EAGAIN)
				continue;
			perror("recvfrom() failed");
			exit(1);
		}

		if (len == 0)
			continue;

		if (0 > (ret = process_message(buffer, len))) {
				perror("unable to process message");
				continue;
		}

		do {
		ret = sendto(sdto, buffer, len, 0, (struct sockaddr *)&sato,
																sizeof(sato));
		} while (ret != len && errno != EINTR);

		if (ret < 0) {
			perror("sendto() failed");
			exit(-1);
		}
	}
}


int main(int argc, char **argv)
{
	memset(&args, 0, sizeof(args));

	argp_parse(&argp, argc, argv, 0, 0, &args);

	run();

	return 0;
}

